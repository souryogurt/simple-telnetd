Overview
--------

simple-telnetd is simplest telnet daemon written in C++ just for fun, and
published for HeadHunter's portfolio. It works atleast in GNU/Linux and FreeBSD.

How to build?
-------------

Clone sources and type:

    autoreconf -if
    ./configure
    make

After that you can launch the binary and test it

Running tests
-------------

To run a tests you need to install libcppunit>=1.12.1.
Debian\Ubuntu:

    sudo apt-get install libcppunit-dev

And than you can type:

    make check

License
-------
Sources are released under WTFPL2.0 . See COPYING file.
