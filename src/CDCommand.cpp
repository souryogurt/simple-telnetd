#include <CDCommand.h>
#include <errno.h>

bool CDCommand::exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const{
	if(args[0] == "cd"){
		int ret;
		if(args.size()>1){
			ret = chdir(args[1].c_str());
			if(ret != 0){
				int err = errno;
				output <<"cd: "<< args[1] << ": ";
				switch( err ){
				case ENOENT:
					output << "No such file or directory\n";
					break;
				case ENOTDIR:
					output << "Not a directory\n";
					break;
				case EACCES:
					output << "Permission denied\n";
					break;
				}
			}
		}else
			ret = chdir(sh.getHomeDirectory().c_str());
		return true;
	}
	return CommandHandler::exec(sh,args,output);
}