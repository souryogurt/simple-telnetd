#ifndef __CDCOMMAND_H__
#define __CDCOMMAND_H__
#include <Shell.h>
#include <vector>
#include <string>
#include <sstream>
#include <CommandHandler.h>

class CDCommand:public CommandHandler
{
	public:
		CDCommand(CommandHandler *handler=0):CommandHandler(handler){};
		bool exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const;
};

#endif /* __CDCOMMAND_H__ */