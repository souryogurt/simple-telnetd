#ifndef __COMMANDHANDLER_H__
#define __COMMANDHANDLER_H__

#include <vector>
#include <string>
#include <sstream>
class Shell;
//Chain of responsibility pattern
class CommandHandler
{
	private:
		CommandHandler *nextHandler;
	public:
		CommandHandler(CommandHandler * handler=0):nextHandler(handler){};
		virtual bool exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const{
			if(nextHandler){
				return nextHandler->exec(sh,args,output);
			}
			return false;
		};
		virtual ~CommandHandler(void){
			if(nextHandler)
				delete nextHandler;
		};
};

#endif /* __COMMANDHANDLER_H__ */