#include <CommandLineParser.h>

const std::string CommandLineParser::readInQuotes(std::string::const_iterator &start, const std::string &command){
	std::string::const_iterator end = ++start;
	std::string result;
	while(end!=command.end() && *end!='"')
		end++;
	if(start!=end)
		result = std::string(start,end);
	start = (end==command.end())?end:++end;
	return result;
}

const std::string CommandLineParser::readLiteral(std::string::const_iterator &start, const std::string &command){
	std::string::const_iterator end = start;
	std::string result;
	while(end!=command.end() && *end!=' ' && *end!='\t')
		end++;
	if(start!=end)
		result=std::string(start,end);
	start=end;
	return result;
}

void CommandLineParser::skipWhitespaces(std::string::const_iterator &start,const std::string &command){
	while((*start==' '  || *start=='\t') && start!=command.end())start++;
}

void CommandLineParser::parse(std::vector<std::string> &args, const std::string &command){
	std::string::const_iterator start = command.begin();
	while(start!=command.end()){
		switch(*start){
			case ' ':
			case '\t':
				skipWhitespaces(start,command);
				break;
			case '"':
				args.push_back(readInQuotes(start,command));
				break;
			default:
				args.push_back(readLiteral(start,command));
		}
		
	}
}
