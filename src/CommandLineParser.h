#ifndef __COMMANDLINEPARSER_H__
#define __COMMANDLINEPARSER_H__
#include <string>
#include <vector>

class CommandLineParser
{
	public:
		static void parse(std::vector<std::string> &args, const std::string &command);
	private:
		CommandLineParser(){};//Static class
		static const std::string readInQuotes(std::string::const_iterator &start, const std::string &command);
		static const std::string readLiteral(std::string::const_iterator &start, const std::string &command);
		static void skipWhitespaces(std::string::const_iterator &start,const std::string &command);
};

#endif /* __COMMANDLINEPARSER_H__ */
