#include <Connection.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <stdexcept>
#include <SocketExceptions.h>

const Connection Connection::TCP(const char *address_string,int port){
	int client = socket(PF_INET,SOCK_STREAM,0);
	if(client==-1){ 
		throw std::runtime_error(strerror(errno));
	}
	struct sockaddr_in address = {0};
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	inet_aton(address_string,static_cast<in_addr*>(&address.sin_addr));
	int result = connect(client,reinterpret_cast<const struct sockaddr*>(&address),sizeof(struct sockaddr_in));
	if(result!=0){
		int err = errno;
		::close(client);
		switch(err){
		case ECONNREFUSED:
			throw ConnectionRefusedException(port);
		case ETIMEDOUT:
			throw ConnectionTimeoutException();
		default:
			throw std::runtime_error(strerror(errno));
		}
	}
	return Connection(client);
}

const Connection& Connection::operator=(const Connection& right){
	Descriptor::operator=(right);
	return *this;
}

int Connection::read(unsigned char * buffer, unsigned int size){
	int result = recv(Handle,buffer,size,NULL);
	if(result==0){
		throw ConnectionAbortedException();
	}else if(result<0){
		throw std::runtime_error(strerror(errno));
	}
	return result;
}

int Connection::write(const unsigned char* buffer, unsigned int size){
	int result = send(Handle,buffer,size,NULL);
	if(result<0){
		int err = errno;
		switch(err){
			case EPIPE:
			case ENOTCONN:
				throw ConnectionAbortedException();
				break;
			default:
				throw std::runtime_error(strerror(errno));	
		}
	}
	return result;
}
