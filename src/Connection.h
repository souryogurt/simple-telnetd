#ifndef __CONNECTIONHEADER_H__
#define __CONNECTIONHEADER_H__
#include <Descriptor.h>


class Connection:public Descriptor
{
	friend class Server;
	public:
		static const Connection TCP(const char*,int);
		Connection(const Connection& right):Descriptor(right){};
		const Connection& operator=(const Connection& right);
		Connection():Descriptor(){};
		
		int read(unsigned char * buffer, unsigned int size);
		int write(const unsigned char* buffer, unsigned int size);
	private:
		Connection (int sock):Descriptor(sock){};
};

#endif /* __CONNECTIONHEADER_H__ */

