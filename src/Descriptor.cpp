#include <Descriptor.h>
#include <unistd.h>

Descriptor::Descriptor(int descriptor){
	Handle = descriptor;
}

Descriptor::Descriptor (const Descriptor& right){
	Handle = dup(right.Handle);
}

Descriptor::~Descriptor(){
	::close(Handle);
}

int Descriptor::getHandle(void) const{
	return Handle;
}

void Descriptor::close(void){
	::close(Handle);
	Handle = -1;
}

const Descriptor& Descriptor::operator =(const Descriptor &right){
	if(Handle != right.Handle){
		::close(Handle);
		Handle = dup(right.Handle);
	}
	return *this;
}
