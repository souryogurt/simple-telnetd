#ifndef __DESCRIPTOR_H__
#define __DESCRIPTOR_H__

class Descriptor
{
	public:
		Descriptor (int descriptor=-1);
		Descriptor (const Descriptor& );
		virtual ~Descriptor();
		int getHandle(void)const;
		const Descriptor& operator =(const Descriptor &right);
		void close(void);
	protected:
		int Handle;
		/* data */
};

#endif /* __DESCRIPTOR_H__ */

