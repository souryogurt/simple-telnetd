#include <ExitCommand.h>

bool ExitCommand::exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const{
	if(args[0] == "exit"){
		sh.exit();
		return true;
	}
	return CommandHandler::exec(sh,args,output);
}