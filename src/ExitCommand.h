#ifndef __EXITCOMMAND_H__
#define __EXITCOMMAND_H__
#include <Shell.h>
#include <vector>
#include <string>
#include <sstream>
#include <CommandHandler.h>

class ExitCommand:public CommandHandler
{
	public:
		ExitCommand(CommandHandler *handler = 0):CommandHandler(handler){};
		bool exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const;
};

#endif /* __EXITCOMMAND_H__ */