#include <InetServerClass.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include <unistd.h>

#include <stdio.h>


InetServer::InetServer():ConnectionsCount(0),fAcceptingConnections(false){
	pthread_mutex_init(&InetServerLock,0);
};

void* InetServer::server_thread(void* inet_server){
	static_cast<InetServer*>(inet_server)->processConnections();
	return 0;
}

void InetServer::processConnections(void){
	int client;
	fd_set fd;
	struct timeval tv;
	bool runned;
	do{
		FD_ZERO(&fd);
		FD_SET(serverSocket,&fd);
		tv.tv_sec=0;
		tv.tv_usec=10000; // timeval struct should be initilized every iteration
                          // couse it may be changed by _select_
		int ret=select(serverSocket+1,&fd,0,0,&tv);
		if(ret==1){
			client=accept(serverSocket,0,0);
			ConnectionsCount++;
		}else if(ret==-1){
			break;
		}
		pthread_mutex_lock(&InetServerLock);
		runned = fAcceptingConnections;
		pthread_mutex_unlock(&InetServerLock);
	}while(runned);
}

unsigned int InetServer::getConnectionsCount(void) const{
	return ConnectionsCount;
}

void InetServer::acceptConnections(unsigned int port){
	serverSocket = socket(PF_INET,SOCK_STREAM,0);
	struct sockaddr_in localaddress = {0};
	localaddress.sin_family = AF_INET;
	localaddress.sin_port = htons(port);
	localaddress.sin_addr.s_addr = INADDR_ANY;
	while(bind(serverSocket,reinterpret_cast<const struct sockaddr*>(&localaddress),sizeof(struct sockaddr_in))!=0){
		usleep(100);
	}
	listen(serverSocket,1);
	pthread_mutex_lock(&InetServerLock);
	fAcceptingConnections = true;
	pthread_mutex_unlock(&InetServerLock);
	pthread_create(&thread_id, NULL, InetServer::server_thread, this);
}

void InetServer::shutdown(void){
	pthread_mutex_lock(&InetServerLock);
		fAcceptingConnections = false; 
	pthread_mutex_unlock(&InetServerLock);
	
	pthread_join(thread_id,0);
	close(serverSocket);
}
