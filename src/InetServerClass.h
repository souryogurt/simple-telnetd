#ifndef __INETSERVERCLASS_H__
#define __INETSERVERCLASS_H__
#include <pthread.h>

class InetServer
{
	private:
		unsigned int ConnectionsCount;
		int serverSocket;
		pthread_t thread_id;
		bool fAcceptingConnections;
		pthread_mutex_t InetServerLock;
		
		static void* server_thread(void*);
		void processConnections(void);
	public:
		InetServer ();
		void acceptConnections(unsigned int port);
		void shutdown(void);
		unsigned int getConnectionsCount(void) const;
};

#endif /* __INETSERVERCLASS_H__ */

