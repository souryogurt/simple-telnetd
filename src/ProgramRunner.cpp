#include <ProgramRunner.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/wait.h> 
#include <errno.h>
#include <libgen.h>

ProgramRunner::ProgramRunner(const char* program, CommandHandler *handler):CommandHandler(handler),programname(program)
{
	char* name = new char[strlen(program)+1];
	strcpy(name, program);
	bname = std::string(basename(name));
	delete [] name;
}

bool ProgramRunner::exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const{
	char *absolutename = realpath(args[0].c_str(),0);
	if(absolutename == 0){
		if(args[0] != bname)
			return CommandHandler::exec(sh,args,output);
	}else{
		if(args[0] != programname && std::string(absolutename)!=programname){
			free(absolutename);
			return CommandHandler::exec(sh,args,output);
		}
	}
	free(absolutename);
	
	int pipes[2];
	if(pipe(pipes)==0){ 
		pid_t pid = fork();
		if(pid == 0){
			//Child
			close(1); 
			close(2); 
			int out=dup(pipes[1]); // dup returns the smallest available descriptor. Exactly 1 in this case
			int err=dup(pipes[1]); 
			close(pipes[1]);
			close(pipes[0]);
			//Building arguments list
			char** pargs = new char*[args.size()+1];
			for(int i=0; i<args.size();i++){
				pargs[i] = new char[args[i].length()+1];
				strcpy(pargs[i],args[i].c_str());
			}
			pargs[args.size()]=NULL; //Last item should be NULL
			execv(programname.c_str(),pargs);
            //Oops. Error here. :(
			int errresult = errno;
            // stdout will be read by parrent process
			printf("%s: %s\n",programname.c_str(), strerror(errresult));
			for(int i=0; i<args.size(); i++)
				delete[] pargs[i];
			delete[] pargs;
			_exit(0);
		}else{
			close(pipes[1]);
			char buffer;
			while(read(pipes[0], &buffer, 1)==1)
				output << buffer;
			close(pipes[0]);
			waitpid(pid,0,0);
		}
	}
	return true;
}
