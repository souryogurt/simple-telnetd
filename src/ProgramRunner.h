#ifndef __PROGRAMRUNNER_H__
#define __PROGRAMRUNNER_H__
#include <Shell.h>
#include <vector>
#include <string>
#include <sstream>
#include <CommandHandler.h>

class ProgramRunner:public CommandHandler
{
	private:
		std::string programname;
		std::string bname;
	public:
		ProgramRunner(const char* program, CommandHandler *handler = 0);
		bool exec(Shell &sh, std::vector<std::string> &args, std::ostringstream &output) const;
};

#endif /* __PROGRAMRUNNER_H__ */