#include <Server.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <stdexcept>
#include <SocketExceptions.h>

const Server Server::TCP(int port, int queueSize){
	int server = socket(PF_INET,SOCK_STREAM,0);
	if(server==-1){ 
		throw std::runtime_error(strerror(errno));
	}
	struct sockaddr_in localaddress = {0};
	localaddress.sin_family = AF_INET;
	localaddress.sin_port = htons(port);
	localaddress.sin_addr.s_addr = INADDR_ANY;
	int bindresult = bind(server,reinterpret_cast<const struct sockaddr*>(&localaddress),sizeof(struct sockaddr_in));
	if(bindresult!=0){
		int e = errno;
		::close(server); 
		switch(e){
			case EADDRINUSE:
				throw BindUsedAddressException(port);
			case EACCES: 
				throw BindHaveNoAccessException(port);
			default:
				throw std::runtime_error(strerror(errno));
		}
	}
	listen(server,queueSize);
	return Server(server);
}

const Connection Server::accept(void) const{
	int socket = ::accept(Handle,0,0);
	if(socket==-1){
		if(errno==EINTR)
			throw InterruptedBySignalException();
		else
			throw std::runtime_error(strerror(errno));
	}
	return Connection(socket);
}

const Server& Server::operator =(const Server &right){
	Descriptor::operator=(right);
	return *this;
}
