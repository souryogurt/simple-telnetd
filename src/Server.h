#ifndef __SERVERSOCKET_H__
#define __SERVERSOCKET_H__
#include <Descriptor.h>
#include <Connection.h>

class Server:public Descriptor
{
	public:
		static const Server TCP(int port, int queueSize);
		const Connection accept(void) const;
		Server():Descriptor(){};
		Server(const Server& right):Descriptor(right){};
		const Server& operator =(const Server &right);
	private:
		Server (int sock):Descriptor(sock){};
};

#endif /* __SERVERSOCKET_H__ */

