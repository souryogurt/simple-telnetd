#include <Shell.h>
#include <sstream>
#include <unistd.h>
#include <limits.h> //PATH_MAX here
#include <vector>
#include <CommandLineParser.h>
#include <CDCommand.h>
#include <ExitCommand.h>
#include <ProgramRunner.h>
#include <fstream>

Shell::Shell(const char * newHomeDirectory):HomeDirectory(newHomeDirectory),works(true),cmdHandler(0){

}

const std::string Shell::getPrompt(void)const{
	return getCurrentDirectory() + std::string("$ ");
}

const std::string Shell::getHomeDirectory(void)const{
	return HomeDirectory;
}

const std::string Shell::getCurrentDirectory(void)const{
	char currentdir[PATH_MAX]={0};
	char* ret = getcwd(currentdir,PATH_MAX);
	return std::string(currentdir);
}

const std::string Shell::exec(const std::string& command){
	std::vector<std::string> args;
	CommandLineParser::parse(args,command); 
	if(args.size()==0)
		return std::string("");
	if(cmdHandler){
		std::ostringstream output;
		if(cmdHandler->exec(*this,args,output))
			return output.str();
	}	
	return args[0] + ": command not found\r\n";
}

bool Shell::isWorks(void)const{
	return works;
}

void Shell::exit(void){
	works=false;
}

CommandHandler* Shell::setCommandHandler(CommandHandler *handler){
	CommandHandler* old = cmdHandler;
	cmdHandler = handler;
	return old;
}

CommandHandler* Shell::loadCommands(const char* config){
	CommandHandler *handler = 0;
	std::string command;
	std::fstream cfg;
	cfg.open(config,std::fstream::in);
	while(cfg.good()){
		std::getline(cfg,command);
		if(command == "cd")
			handler = new CDCommand(handler);
		else if(command == "exit")
			handler = new ExitCommand(handler);
		else
			handler = new ProgramRunner(command.c_str(),handler);
	}
	cfg.close();
	return handler;
}
