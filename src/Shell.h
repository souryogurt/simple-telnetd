#ifndef __SHELL_H__
#define __SHELL_H__
#include <string>
class CommandHandler;

class Shell
{
	public:
		explicit Shell(const char* newHomeDirectory);
		const std::string getPrompt(void)const;
		const std::string getHomeDirectory(void)const;
		const std::string getCurrentDirectory(void)const;
		const std::string exec(const std::string& command);
		bool isWorks(void)const;
		void exit(void);
		CommandHandler* setCommandHandler(CommandHandler *handler);
		static CommandHandler* loadCommands(const char* config);
	private:
		bool works;
		std::string HomeDirectory;
		CommandHandler *cmdHandler;
};

#endif /* __SHELL_H__ */