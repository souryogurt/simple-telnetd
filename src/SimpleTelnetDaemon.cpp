#include <SimpleTelnetDaemon.h>

#include <getopt.h>
#include <stdlib.h>
#include <config.h>
using namespace std;

int SimpleTelnetDaemon::ChildTerminatedSignal = 0;
int SimpleTelnetDaemon::SighupSignal = 0;

void SimpleTelnetDaemon::SignalHandler(int signal)
{
	switch(signal){
	case SIGCHLD:
		SimpleTelnetDaemon::ChildTerminatedSignal++;
		break;
	case SIGHUP:
		SimpleTelnetDaemon::SighupSignal++;
		break;
	}
}

SimpleTelnetDaemon::SimpleTelnetDaemon(){
	port = 23;
	configFilename = string("/etc/simple-telnetd.conf");
	commandHandlerChain = NULL;
}

void SimpleTelnetDaemon::printUsage(void){
	cout<<PACKAGE_STRING<< endl<< "mailto: <"<<PACKAGE_BUGREPORT<<">"<<endl;
	cout<<"Arguments:"<<endl;
	cout<<"-h --help \t\t Print this help"<<endl;
	cout<<"-p NUM --port NUM \t Set port number to bind (default: 23)"<<endl;
	cout<<"-c FILE --config FILE \t Read configuration from FILE (default: /etc/simple-telnetd.conf)"<<endl;
}

bool SimpleTelnetDaemon::processArguments(int argc, char** argv){
	const char* const short_options = "hp:c:";
	const struct option long_options[]={
		{"help",	0, NULL, 'h'},
		{"port",	1, NULL, 'p'},
		{"config",	1, NULL, 'c'},
		{NULL,		0, NULL, 0  }
	};
	int next_option;
	opterr = 0;
	do{
		next_option = getopt_long(argc,argv, short_options, long_options, NULL);
		switch(next_option){
			case 'h':
				printUsage();
				return false;
			case 'p':
				port = atol(optarg);
				break;
			case 'c':
				configFilename = string(optarg);
				break;
			case '?':
				printUsage();
				return false;
			case -1:
				break;
			default:
				abort();
		}
	}while(next_option != -1);
	return true;
}

bool SimpleTelnetDaemon::readConfiguration(void)
{	
	CommandHandler *newChain = Shell::loadCommands(configFilename.c_str());
	if(newChain == NULL){
		cerr<<"Can't load commands list from "<<configFilename<<endl;
		return false;
	}
	if(commandHandlerChain!=NULL)
		delete commandHandlerChain;
	commandHandlerChain = newChain;
}

bool SimpleTelnetDaemon::Initialization(int argc, char** argv)
{
	if(not processArguments(argc,argv))
		return false;
	if(not readConfiguration())
		return false;
	try{
		server = Server::TCP(port,1);
	}catch(BindException &e){
		cout << e.what() << endl;
		return false;
	}
	return true;
}

bool SimpleTelnetDaemon::daemonize(){
	if(fork()>0)
		return false;
	setsid();//Creating separate process group
	close(0);close(1);close(2);//Daemong can't read and write to/from std channels
	int i0=open("/dev/null",O_RDWR);
	int i1=dup(i0);
	int i2=dup(i0);
	umask(027);
	return true;
}


void SimpleTelnetDaemon::setSignalHandlers(void){
	struct sigaction act={0};
	act.sa_handler = SimpleTelnetDaemon::SignalHandler;
	sigemptyset(&act.sa_mask);
	sigaction(SIGCHLD,&act,0);
	sigaction(SIGHUP,&act,0);
}

bool SimpleTelnetDaemon::handleSignals(void){
	while(SimpleTelnetDaemon::ChildTerminatedSignal !=0 || SimpleTelnetDaemon::SighupSignal!=0){
		if(SimpleTelnetDaemon::ChildTerminatedSignal>0){
			int child = wait(0); 
			if(child>=0){
				Sessions.remove(child);
				SimpleTelnetDaemon::ChildTerminatedSignal--;
			}
		}
		if(SimpleTelnetDaemon::SighupSignal>0){
			readConfiguration();
			SimpleTelnetDaemon::SighupSignal --;
		}
	}
	return true;
}

void SimpleTelnetDaemon::run(int argc, char** argv)
{
	if((not Initialization(argc,argv)) || (not daemonize()))
		return;
	
	setSignalHandlers(); 
	
	while(1){
		while(handleSignals()){
			Connection client;
			try{
				client = server.accept(); 
			}catch(InterruptedBySignalException &e){ 
				continue;
			}
			Telnet telnet(client);
			int pid = fork();
			if(pid ==0 ){ 
				server.close(); 
				Shell sh("/");
				sh.setCommandHandler(commandHandlerChain);
				while(sh.isWorks()){
					string command;
					try{
						telnet<<sh.getPrompt();
						telnet>>command;
						telnet<<sh.exec(command);
					}catch(...){
						sh.exit();
					}
				}
				_exit(0);
			}
			Sessions.push_back(pid);
		}
	}
}
