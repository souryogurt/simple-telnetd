#ifndef __SIMPLETELNETDDAEMON_H__
#define __SIMPLETELNETDDAEMON_H__
#include <Server.h>
#include <Shell.h>
#include <SocketExceptions.h>
#include <Telnet.h>
#include <iostream>
#include <list>
#include <sys/types.h>
#include <sys/stat.h> 
#include <sys/wait.h>
#include <signal.h> 
#include <fcntl.h>
#include <CommandHandler.h>

class SimpleTelnetDaemon{
	public:
		SimpleTelnetDaemon();
		void run(int argc, char** argv);
		static void SignalHandler(int signal);
		static int ChildTerminatedSignal;
		static int SighupSignal;
	private:
		int port;
		Server server;
		CommandHandler *commandHandlerChain;
		std::list<int> Sessions;
		std::string configFilename;
		bool daemonize();
		bool Initialization(int argc, char** argv);
		bool processArguments(int argc, char** argv);
		void setSignalHandlers(void);
		bool handleSignals(void);
		void printUsage(void);
		bool readConfiguration(void);
};

#endif /* __SIMPLETELNETDDAEMON_H__ */

