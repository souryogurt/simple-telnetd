#include <SocketExceptions.h>
#include <string>
#include <sstream>
#include <iostream>

BindException::BindException(int port):Port(port),std::runtime_error("Can't bind socket."){
	msg=runtime_error::what(); 
};

const char* BindException::what( ) const throw(){
	return msg.c_str();
};


//BindUsedAddressException реализация
BindUsedAddressException::BindUsedAddressException(int port):BindException(port){
	std::ostringstream result;
	result << msg << " " << Port << " port already in use.";
	msg = result.str();
};


//BindHaveNoAccessException реализация

BindHaveNoAccessException::BindHaveNoAccessException(int port):BindException(port){
	std::ostringstream result;
	result << msg << " You have no access to bind "<< Port << " port.";
	msg = result.str();
};

//ConnectionRefusedException
ConnectionRefusedException::ConnectionRefusedException(int port):ConnectionException("Connection refused."),Port(port){
	std::ostringstream result;
	result << std::runtime_error::what() << " Remote port "<< Port << " closed.";
	msg = result.str();
};

const char* ConnectionRefusedException::what( ) const throw(){
	return msg.c_str();
};

//ConnectionRefusedException
ConnectionTimeoutException::ConnectionTimeoutException():ConnectionException("Connection timeout."){
	
};

ConnectionAbortedException::ConnectionAbortedException():ConnectionException("Connection aborted"){

};
