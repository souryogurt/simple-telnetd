#ifndef __SOCKETEXCEPTIONS_H__
#define __SOCKETEXCEPTIONS_H__
#include <stdexcept>

class BindException:public std::runtime_error{
	protected:
		int Port;
		std::string msg;
	public:
		BindException(int port);
		virtual ~BindException(void)throw(){};
		virtual const char *what( ) const throw();
};

class BindUsedAddressException:public BindException{
	public:
		BindUsedAddressException(int port);

};


class BindHaveNoAccessException:public BindException{
	public:
		BindHaveNoAccessException(int port);
};

class ConnectionException:public std::runtime_error{
	public:
		ConnectionException(const char* msg):std::runtime_error(msg){};
};


class ConnectionRefusedException:public ConnectionException{
	protected:
		int Port;
		std::string msg;
	public:
		ConnectionRefusedException(int port);
		virtual const char *what( ) const throw();
		virtual ~ConnectionRefusedException(void)throw(){};
};

class ConnectionTimeoutException:public ConnectionException{
	public:
		ConnectionTimeoutException();

};

class ConnectionAbortedException:public ConnectionException{
	public:
		ConnectionAbortedException();
};

class InterruptedBySignalException:public std::runtime_error{
	public:
		InterruptedBySignalException():std::runtime_error("System call interrupted by a signal"){};
};


#endif /* __SOCKETEXCEPTIONS_H__ */

