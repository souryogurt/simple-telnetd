#include <Telnet.h>
#include <sstream>
#include <iostream>
#include <string.h>

const char* const Telnet::endl="\r\n";

Telnet& Telnet::operator>>(std::string &result){
	unsigned char GA[]={0xFF,0xF9};// "GoAhead" command.
	write(GA,2);
	unsigned char *buf=new unsigned char[MaxLineLength+1];
	memset(buf,0,MaxLineLength+1);
	try{
		readChuck(buf,0);
	}catch(...){
		delete[] buf;
		throw;
	}
	result = std::string(reinterpret_cast<char*>(buf));
	delete[] buf;
	return *this;
}

void Telnet::readChuck(unsigned char *buffer,int currentSize){
	if(currentSize>=MaxLineLength)return;
	read(&buffer[currentSize],1);
	switch(buffer[currentSize]){
		case '\n':
			if(currentSize>0 && buffer[currentSize-1] == '\r'){
				buffer[currentSize-1]=0;
				return;
			}
			break;
		case 0xFF:
			if(processCommand()){
				buffer[currentSize]=0;
				currentSize--;
			}
			break;
		case 0:
			currentSize--; //Пропускаем нули
	}
	readChuck(buffer,++currentSize);
}


/* To support NVT we must discard all options */
bool Telnet::processCommand(void){
	unsigned char cmd;
	unsigned char resp[3]={0xFF,0xFE,cmd};
	read(&cmd,1);
	switch(cmd){
		case 0xFF:// It's just a 0xFF symbol. Nothing special
			return false;
		case 0xFB: // WILL option 
			read(&cmd,1); 
			resp[1]=0xFE; 
			resp[2]=cmd;
			write(resp,3); 
			break;
		case 0xFD: //DO option
			read(&cmd,1);
			resp[1]=0xFC; 
			resp[2]=cmd;
			write(resp,3);
			break;
		case 0xFC: 
			read(&cmd,1);
			break;
		case 0xFE:
			read(&cmd,1);
			break;
	}
	return true;
}

const Telnet& Telnet::operator=(const Telnet& right){
	Connection::operator=(right);
	MaxLineLength = right.MaxLineLength;
	return *this;
}

Telnet& Telnet::operator<<(const std::string &str){
	unsigned char addc;
	for(std::string::const_iterator i = str.begin(); i<str.end(); i++){
		unsigned char c = *i;
		switch(c){
			case 0xFF:
				write(&c,1);
				break;
			case '\n':
				if(*(i-1)!='\r'){
					addc='\r';
					write(&addc,1);
				}
				break;
		}
		write(&c,1);
	}
	return *this;
}


unsigned int Telnet::getMaxLineLength(void) const{
	return MaxLineLength;
}
void Telnet::setMaxLineLength(unsigned int newLength){
	MaxLineLength = newLength;
}
