#ifndef __TELNET_H__
#define __TELNET_H__
#include <string>
#include <Connection.h>

class Telnet:public Connection
{
	public:
		static const char* const endl;
		Telnet (const Telnet& right):Connection(right),MaxLineLength(160){};
		explicit Telnet (const Connection& right):Connection(right),MaxLineLength(160){};
		Telnet ():Connection(),MaxLineLength(160){};
		const Telnet& operator=(const Telnet& right);
		Telnet& operator>>(std::string &);
		Telnet& operator<<(const std::string &);
		unsigned int getMaxLineLength(void) const;
		void setMaxLineLength(unsigned int);
	private:
		unsigned int MaxLineLength;
		void readChuck(unsigned char *buffer,int currentSize);
		bool processCommand(void);
};

#endif /* __TELNET_H__ */

