#include "CDCommandTest.h"
#include <Shell.h>
#include <CommandLineParser.h>
#include <CommandHandler.h>


CPPUNIT_TEST_SUITE_REGISTRATION( CDCommandTest );


void CDCommandTest::setUp()
{
	output.str("");
	output.clear();
	sh = new Shell("/home/yoyo");
	args.clear();
}


void CDCommandTest::tearDown()
{
	delete sh;
}

void CDCommandTest::testExec(){
	CommandLineParser::parse(args, "cd /home");
	CPPUNIT_ASSERT_EQUAL(cd.exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(sh->getCurrentDirectory(),std::string("/home"));
	
	args.clear();
	CommandLineParser::parse(args, "iamnotcd");
	CPPUNIT_ASSERT_EQUAL(cd.exec(*sh,args,output),false);
	CommandHandler &cmd = cd;
	
}

void CDCommandTest::testNotADirectory()
{
	CommandLineParser::parse(args, "cd /bin/sh");
	cd.exec(*sh,args,output);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("cd: /bin/sh: Not a directory\n"));
}
	
void CDCommandTest::testNoSuchFile()
{
	CommandLineParser::parse(args, "cd /notexists");
	CPPUNIT_ASSERT_EQUAL(cd.exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("cd: /notexists: No such file or directory\n"));
}

void CDCommandTest::testPermissionDenied()
{
	CommandLineParser::parse(args, "cd /lost+found");
	cd.exec(*sh,args,output);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("cd: /lost+found: Permission denied\n"));
}

void CDCommandTest::testDefaultBehavior()
{
	CommandLineParser::parse(args, "cd");
	CPPUNIT_ASSERT_EQUAL(cd.exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(sh->getCurrentDirectory(),sh->getHomeDirectory());
}