#ifndef __CDCOMMANDTEST_H__
#define __CDCOMMANDTEST_H__

#include <cppunit/extensions/HelperMacros.h>
#include <CDCommand.h>
class Shell;

class CDCommandTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( CDCommandTest );
	CPPUNIT_TEST( testExec );
	CPPUNIT_TEST( testNotADirectory );
	CPPUNIT_TEST( testNoSuchFile );
	CPPUNIT_TEST( testPermissionDenied );
	CPPUNIT_TEST( testDefaultBehavior );
	CPPUNIT_TEST_SUITE_END();
	private:
		std::ostringstream output;
		Shell* sh;
		std::vector<std::string> args;
		CDCommand cd;
public:
	void setUp();
	void tearDown();
	
	void testExec();
	void testNotADirectory();
	void testNoSuchFile();
	void testPermissionDenied();
	void testDefaultBehavior();
};

#endif /* __CDCOMMANDTEST_H__ */