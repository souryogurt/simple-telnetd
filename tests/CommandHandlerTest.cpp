#include "CommandHandlerTest.h"
#include <Shell.h>
#include <CommandLineParser.h>
#include <sstream>
#include <ExitCommand.h>
#include <CDCommand.h>

#include <CommandHandler.h>

CPPUNIT_TEST_SUITE_REGISTRATION( CommandHandlerTest );


void CommandHandlerTest::setUp()
{	
}


void CommandHandlerTest::tearDown()
{
}

void CommandHandlerTest::testConstructingChain(){
	CommandHandler *handler = 0;
	handler = new CDCommand(handler);
	handler = new ExitCommand(handler);
	
	std::vector<std::string> args;
	std::ostringstream o;
	Shell sh("/home/yoyo");
	CommandLineParser::parse(args,"cd /");
	
	CPPUNIT_ASSERT(handler->exec(sh,args,o));
	delete handler;
}