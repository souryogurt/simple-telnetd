#ifndef __COMMANDHANDLERTEST_H__
#define __COMMANDHANDLERTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class CommandHandlerTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( CommandHandlerTest );
	CPPUNIT_TEST( testConstructingChain );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();
	
	void testConstructingChain();
};

#endif /* __COMMANDHANDLERTEST_H__ */