#include "CommandLineParserTest.h"
#include <CommandLineParser.h>
#include <string>
#include <vector>

CPPUNIT_TEST_SUITE_REGISTRATION( CommandLineParserTest );


void CommandLineParserTest::setUp()
{

}


void CommandLineParserTest::tearDown()
{

}

void CommandLineParserTest::testParsing(){
	std::vector<std::string> args;
	CommandLineParser::parse(args,std::string("  \t \t  first \t \t  \" second long argument\"  \t"));
	CPPUNIT_ASSERT_EQUAL(args[0],std::string("first"));
	CPPUNIT_ASSERT_EQUAL(args[1],std::string(" second long argument"));
}