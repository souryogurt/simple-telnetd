#ifndef __COMMANDLINEPARSERTEST_H__
#define __COMMANDLINEPARSERTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class CommandLineParserTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( CommandLineParserTest );
	CPPUNIT_TEST( testParsing );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();
	
	void testParsing();
};

#endif /* __COMMANDLINEPARSERTEST_H__ */