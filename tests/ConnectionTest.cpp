#include "ConnectionTest.h"
#include <Server.h>
#include <Connection.h>
#include <SocketExceptions.h>
#include <Descriptor.h>

CPPUNIT_TEST_SUITE_REGISTRATION( ConnectionTest );


void ConnectionTest::setUp()
{
	currentPort = 10100;
	while(server.getHandle()==-1){
		try{
			server = Server::TCP(currentPort,1);
		}catch(BindUsedAddressException &e){
			currentPort++;
		}
	}
}


void ConnectionTest::tearDown()
{
	server.close();
}


void ConnectionTest::testCreation()
{
	//Должны безпроблемно подключиться
	CPPUNIT_ASSERT_NO_THROW(Connection c = Connection::TCP("127.0.0.1",currentPort));
	
	CPPUNIT_ASSERT_THROW(Connection c = Connection::TCP("127.0.0.1",21215), ConnectionRefusedException);
	try{
		Connection c = Connection::TCP("127.0.0.1",21215);
	}catch(ConnectionRefusedException &e){
		CPPUNIT_ASSERT_EQUAL(std::string(e.what()),std::string("Connection refused. Remote port 21215 closed."));
	}
}

void ConnectionTest::testReadingAndWriting(){
	Connection remote  = Connection::TCP("127.0.0.1",currentPort);
	Connection client = server.accept();
	unsigned char test1[]="TESTNOP";
	CPPUNIT_ASSERT_EQUAL(remote.write(test1,7),7);
	unsigned char buf[5];
	CPPUNIT_ASSERT_EQUAL(client.read(buf,4),4);
	buf[4]=0;
	CPPUNIT_ASSERT_EQUAL(std::string("TEST"),std::string(reinterpret_cast<char*>(buf)));
}

void ConnectionTest::testExceptions(){

	Connection remote  = Connection::TCP("127.0.0.1",currentPort);
	Connection client = server.accept();
	remote.close(); //Удалённый клиент закрыл подключение
	unsigned char buf;
	CPPUNIT_ASSERT_THROW(client.read(&buf,1),ConnectionAbortedException);
}
