#ifndef __CONNECTIONTEST_H__
#define __CONNECTIONTEST_H__

#include <cppunit/extensions/HelperMacros.h>
#include <Server.h>

class ConnectionTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ConnectionTest );
	CPPUNIT_TEST( testCreation );
	CPPUNIT_TEST( testReadingAndWriting );
	CPPUNIT_TEST( testExceptions );
	CPPUNIT_TEST_SUITE_END();
private:
	Server server;
	int currentPort;
public:
	void setUp();
	void tearDown();
	void testCreation();
	void testReadingAndWriting();
	void testExceptions();
	
};

#endif /* __CONNECTIONTEST_H__ */

