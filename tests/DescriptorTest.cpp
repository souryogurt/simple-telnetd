#include "DescriptorTest.h"
#include <Descriptor.h>


CPPUNIT_TEST_SUITE_REGISTRATION( DescriptorTest );


void DescriptorTest::setUp()
{
}


void DescriptorTest::tearDown()
{
}


void DescriptorTest::testCreation()
{
	Descriptor descriptor;
	CPPUNIT_ASSERT_EQUAL(descriptor.getHandle(),-1);
}

void DescriptorTest::testClosing()
{
	Descriptor d1(123);
	CPPUNIT_ASSERT_EQUAL(d1.getHandle(),123);
	d1.close();
	CPPUNIT_ASSERT_EQUAL(d1.getHandle(),-1);
}
