#ifndef __DESCRIPTORTEST_H__
#define __DESCRIPTORTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class DescriptorTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( DescriptorTest );
	CPPUNIT_TEST( testCreation );
	CPPUNIT_TEST( testClosing );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();
	void testCreation();
	void testClosing();
};

#endif /* __DESCRIPTORTEST_H__ */

