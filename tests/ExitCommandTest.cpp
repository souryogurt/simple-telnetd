#include "ExitCommandTest.h"
#include <Shell.h>
#include <CommandLineParser.h>
#include <sstream>
#include <ExitCommand.h>
#include <CommandHandler.h>

CPPUNIT_TEST_SUITE_REGISTRATION( ExitCommandTest );


void ExitCommandTest::setUp()
{	
}


void ExitCommandTest::tearDown()
{
}

void ExitCommandTest::testExec(){
	Shell sh("/home/yoyo");
	std::vector<std::string> args;
	CommandLineParser::parse(args,"exit");
	ExitCommand ex;
	std::ostringstream output;
	CPPUNIT_ASSERT_EQUAL(ex.exec(sh,args,output),true);
	CPPUNIT_ASSERT(not sh.isWorks());
	CommandHandler &cmd = ex;
}