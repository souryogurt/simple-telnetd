#ifndef __EXITCOMMANDTEST_H__
#define __EXITCOMMANDTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class ExitCommandTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ExitCommandTest );
	CPPUNIT_TEST( testExec );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();
	
	void testExec();
};

#endif /* __EXITCOMMANDTEST_H__ */