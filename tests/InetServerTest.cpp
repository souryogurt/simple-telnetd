#include "InetServerTest.h"
#include <InetServerClass.h>

#include <sys/socket.h>
#include <arpa/inet.h>

CPPUNIT_TEST_SUITE_REGISTRATION( InetServerTest );


bool can_connect(const char* const address, int port){
	bool result = false;
	int client = socket(PF_INET,SOCK_STREAM,0);
	struct sockaddr_in localaddress = {0};
	localaddress.sin_family = AF_INET;
	localaddress.sin_port = htons(port);
	inet_aton(address,static_cast<in_addr*>(&localaddress.sin_addr));
	result = connect(client,reinterpret_cast<const struct sockaddr*>(&localaddress),sizeof(struct sockaddr_in))==0;
	close(client);
	return result;
}
int try_connect(const char* const address, int port){
	int client = socket(PF_INET,SOCK_STREAM,0);
	struct sockaddr_in localaddress = {0};
	localaddress.sin_family = AF_INET;
	localaddress.sin_port = htons(port);
	inet_aton(address,static_cast<in_addr*>(&localaddress.sin_addr));
	if(connect(client,reinterpret_cast<const struct sockaddr*>(&localaddress),sizeof(struct sockaddr_in))==0){
		return client;
	}
	close(client);
	return 0;
}

void InetServerTest::setUp()
{
}


void InetServerTest::tearDown()
{
}


void InetServerTest::testAcceptingConnections()
{
	InetServer server;
	server.acceptConnections(10101); //Принимает подключения на 10101 порту
	CPPUNIT_ASSERT_EQUAL(server.getConnectionsCount(),static_cast<unsigned int>(0));
	
	int client;
	CPPUNIT_ASSERT((client=try_connect("127.0.0.1",10101))!=0);
	usleep(100);
	CPPUNIT_ASSERT_EQUAL(server.getConnectionsCount(),static_cast<unsigned int>(1));
	close(client);
	
	server.shutdown();
	CPPUNIT_ASSERT(can_connect("127.0.0.1",10101)==false);
}
