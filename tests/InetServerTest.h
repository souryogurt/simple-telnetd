#ifndef __INETSERVERTEST_H__
#define __INETSERVERTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class InetServerTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( InetServerTest );
	CPPUNIT_TEST( testAcceptingConnections );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();

	void testAcceptingConnections();
	
};

#endif /* __INETSERVERTEST_H__ */

