#include "ProgramRunnerTest.h"
#include <Shell.h>
#include <CommandLineParser.h>
#include <sstream>
#include <CommandHandler.h>
#include <ProgramRunner.h>

CPPUNIT_TEST_SUITE_REGISTRATION( ProgramRunnerTest );


void ProgramRunnerTest::setUp()
{	
	output.str("");
	output.clear();
	sh = new Shell("/home/yoyo");
	args.clear();
	pr= new ProgramRunner("/bin/uname");
}


void ProgramRunnerTest::tearDown()
{
	delete sh;
	delete pr;
}

void ProgramRunnerTest::testExec(){
	CommandLineParser::parse(args,"/bin/uname");
	CPPUNIT_ASSERT_EQUAL(pr->exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("Linux\n"));
}

void ProgramRunnerTest::testExecWithArgs()
{
	CommandLineParser::parse(args,"/bin/uname -o");
	CPPUNIT_ASSERT_EQUAL(pr->exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("GNU/Linux\n"));
}

void ProgramRunnerTest::testDoesNotExists(){
	ProgramRunner notexists("/bin/notexists");
	CommandLineParser::parse(args,"/bin/notexists");
	CPPUNIT_ASSERT_EQUAL(notexists.exec(*sh,args,output),false);
}

void ProgramRunnerTest::testNotAExecutableFile(){
	ProgramRunner notexe("/etc/passwd");
	CommandLineParser::parse(args,"/etc/passwd");
	CPPUNIT_ASSERT_EQUAL(notexe.exec(*sh,args,output),true);
}

void ProgramRunnerTest::testExecByName()
{
	CommandLineParser::parse(args,"uname");
	CPPUNIT_ASSERT_EQUAL(pr->exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("Linux\n"));
}

void ProgramRunnerTest::testExecByRelativePath()
{
	CommandLineParser::parse(args,"../../../../bin/uname");
	CPPUNIT_ASSERT_EQUAL(pr->exec(*sh,args,output),true);
	CPPUNIT_ASSERT_EQUAL(output.str(),std::string("Linux\n"));
}

void ProgramRunnerTest::testExecByFooRelativePath()
{
	CommandLineParser::parse(args,"bin/../sbin/../uname");
	CPPUNIT_ASSERT_EQUAL(pr->exec(*sh,args,output),false);
}