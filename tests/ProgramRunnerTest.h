#ifndef __PROGRAMRUNNERTEST_H__
#define __PROGRAMRUNNERTEST_H__

#include <cppunit/extensions/HelperMacros.h>
#include <Shell.h>
#include <ProgramRunner.h>

class ProgramRunnerTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ProgramRunnerTest );
	CPPUNIT_TEST( testExec );
	CPPUNIT_TEST( testExecWithArgs );
	CPPUNIT_TEST( testDoesNotExists );
	CPPUNIT_TEST( testNotAExecutableFile );
	CPPUNIT_TEST( testExecByName );
	CPPUNIT_TEST( testExecByRelativePath );
	CPPUNIT_TEST( testExecByFooRelativePath );
	CPPUNIT_TEST_SUITE_END();
private:
		std::ostringstream output;
		Shell* sh;
		std::vector<std::string> args;
		ProgramRunner *pr;
public:
	void setUp();
	void tearDown();
	
	void testExec();
	void testExecWithArgs();
	void testDoesNotExists();
	void testNotAExecutableFile();
	void testExecByName();
	void testExecByRelativePath();
	void testExecByFooRelativePath();
};

#endif /* __PROGRAMRUNNERTEST_H__ */