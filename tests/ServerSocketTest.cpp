#include "ServerSocketTest.h"
#include <Server.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <SocketExceptions.h>
#include <Connection.h>
#include <Descriptor.h>
#include <string>
#include <sstream>
#include <iostream>

CPPUNIT_TEST_SUITE_REGISTRATION( ServerSocketTest );


void ServerSocketTest::setUp()
{
	currentPort = 10102;
	while(server.getHandle()==-1){
		try{
			server = Server::TCP(currentPort,5);
		}catch(BindUsedAddressException &e){
			currentPort++;
		}
	}
}


void ServerSocketTest::tearDown()
{
	server.close();
}

void ServerSocketTest::testAlreadyInUse(){

	//Создание второго сокета на томже порту вызывает исключение Address already in use.
	CPPUNIT_ASSERT_THROW(Server server2 = Server::TCP(currentPort,5),BindUsedAddressException);
	
	//Проверяем сообщение исключения Address already in use
	try{
		Server server2 = Server::TCP(currentPort,5);
	}catch(const BindUsedAddressException &e){
		std::ostringstream result;
		result << "Can't bind socket. " << currentPort << " port already in use.";
		CPPUNIT_ASSERT_EQUAL(std::string(e.what()),result.str());
	}
}

void ServerSocketTest::testHaveNoAccess(){
	//Создание слушающего сокета на порту <1024 Вызывает исключение "Нет прав."
	CPPUNIT_ASSERT_THROW(Server server2 = Server::TCP(10,5),BindHaveNoAccessException);
	
	//Проверяем сообщение исключения "Нет прав".
	try{
		Server server2 = Server::TCP(10,5);
	}catch(const BindHaveNoAccessException &e){
		CPPUNIT_ASSERT_EQUAL(std::string(e.what()),std::string("Can't bind socket. You have no access to bind 10 port."));
	}
}

void ServerSocketTest::testCreation()
{
	CPPUNIT_ASSERT(server.getHandle()!=-1);
	
	//Проверяем получается ли подключение
	CPPUNIT_ASSERT_NO_THROW(Connection c = Connection::TCP("127.0.0.1",currentPort));
}

void ServerSocketTest::testAcceptingConnection()
{
	Connection remote = Connection::TCP("127.0.0.1",currentPort);
	Connection client = server.accept();
	CPPUNIT_ASSERT(client.getHandle()!=-1);	
	
}
