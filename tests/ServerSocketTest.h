#ifndef __SERVERSOCKETTEST_H__
#define __SERVERSOCKETTEST_H__

#include <cppunit/extensions/HelperMacros.h>

#include <Server.h>

class ServerSocketTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ServerSocketTest );
	CPPUNIT_TEST( testCreation );
	CPPUNIT_TEST( testHaveNoAccess );
	CPPUNIT_TEST( testAlreadyInUse );
	CPPUNIT_TEST( testAcceptingConnection );
	CPPUNIT_TEST_SUITE_END();
private:
	Server server;
	int currentPort;
public:
	void setUp();
	void tearDown();

	void testCreation();
	void testHaveNoAccess();
	void testAlreadyInUse();
	void testAcceptingConnection();
};

#endif /* __SERVERSOCKETTEST_H__ */

