#include "ShellTest.h"
#include <unistd.h>
#include <string>
#include <sstream>
#include <iostream>
#include <Shell.h>
#include <CommandHandler.h>
#include <ExitCommand.h>
#include <CDCommand.h>

CPPUNIT_TEST_SUITE_REGISTRATION( ShellTest );


void ShellTest::setUp()
{
}


void ShellTest::tearDown()
{
}


void ShellTest::testPrompt()
{
	Shell sh("/home/yoyo");
	
	CPPUNIT_ASSERT_EQUAL(sh.getHomeDirectory(),std::string("/home/yoyo"));
	
	char currentdir[4096]={0};
	char* result = getcwd(currentdir,4096);
	
	CPPUNIT_ASSERT_EQUAL(sh.getCurrentDirectory(),std::string(currentdir));
	
	std::ostringstream s;
	s<<currentdir<<"$ ";
	CPPUNIT_ASSERT_EQUAL(s.str(),sh.getPrompt());
}


void ShellTest::testCDCommand()
{
	CommandHandler *handler = 0;
	handler = new ExitCommand(handler);
	handler = new CDCommand(handler);
	Shell sh("/home/yoyo");
	sh.setCommandHandler(handler);
	
	CPPUNIT_ASSERT_EQUAL(sh.exec("cd /"),std::string(""));
	CPPUNIT_ASSERT_EQUAL(sh.getCurrentDirectory(),std::string("/"));

	CPPUNIT_ASSERT_EQUAL(sh.exec("notcd"),std::string("notcd: command not found\r\n"));
	CPPUNIT_ASSERT(sh.getCurrentDirectory()!=sh.getHomeDirectory());
	
	CPPUNIT_ASSERT_EQUAL(sh.exec(""),std::string(""));
	delete handler;
}

void ShellTest::testExit()
{
	Shell sh("/home/yoyo");
	CPPUNIT_ASSERT(sh.isWorks());
	sh.exit();
	CPPUNIT_ASSERT(not sh.isWorks());
}

void ShellTest::testHandlerChain()
{
	CommandHandler *handler = 0;
	handler = new ExitCommand(handler);
	handler = new CDCommand(handler);
	
	Shell sh("/home/yoyo");
	CPPUNIT_ASSERT_EQUAL(sh.setCommandHandler(handler),(CommandHandler*)0);
	CPPUNIT_ASSERT(sh.isWorks());
	sh.exec("exit");
	CPPUNIT_ASSERT(not sh.isWorks());
	delete handler;
}

void ShellTest::testLoadingCommandHandlerChain()
{
	CommandHandler * handler = Shell::loadCommands("/tmp/simple-telnetd.conf");
	CPPUNIT_ASSERT(handler !=0);
	Shell sh("/home/yoyo");
	sh.setCommandHandler(handler);
	CPPUNIT_ASSERT(sh.exec("cd") != std::string("cd: command not found"));
	CPPUNIT_ASSERT(sh.exec("ls") != std::string("ls: command not found"));
	CPPUNIT_ASSERT(sh.exec("uname") != std::string("uname: command not found"));
	CPPUNIT_ASSERT(sh.exec("exit") != std::string("exit: command not found"));
	CommandHandler *zerohandler = Shell::loadCommands("/notexists");
	CPPUNIT_ASSERT_EQUAL(zerohandler,(CommandHandler*)0);
}