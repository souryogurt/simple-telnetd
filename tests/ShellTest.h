#ifndef __SHELLTEST_H__
#define __SHELLTEST_H__

#include <cppunit/extensions/HelperMacros.h>

class ShellTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( ShellTest );
	CPPUNIT_TEST( testPrompt );
	CPPUNIT_TEST( testCDCommand );
	CPPUNIT_TEST( testExit );
	CPPUNIT_TEST( testHandlerChain );
	CPPUNIT_TEST( testLoadingCommandHandlerChain );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();
	void tearDown();
	
	void testPrompt();
	void testCDCommand();
	void testExit();
	void testHandlerChain();
	void testLoadingCommandHandlerChain();
};

#endif /* __SHELLTEST_H__ */