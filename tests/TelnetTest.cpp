#include "TelnetTest.h"
#include <Telnet.h>
#include <stdio.h>
#include <string.h>

CPPUNIT_TEST_SUITE_REGISTRATION( TelnetTest );


void TelnetTest::setUp()
{
	currentPort = 10102;
	while(server.getHandle()==-1){
		try{
			server = Server::TCP(currentPort,5);
		}catch(BindUsedAddressException &e){
			currentPort++;
		}
	}
	remote = Connection::TCP("127.0.0.1",currentPort);
	client = server.accept();
}


void TelnetTest::tearDown()
{
	remote.close();
	client.close();
	server.close();
}


void TelnetTest::testReading()
{
	Telnet telnet(client);
	unsigned char test1[]="this\r is the test\r\nsecond\r\n";
	remote.write(test1,27);
	std::string s1,s2;
	telnet >> s1 >> s2;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("this\r is the test"));
	CPPUNIT_ASSERT_EQUAL(s2,std::string("second"));
	unsigned char response[4]={0};
	remote.read(response,4);
	CPPUNIT_ASSERT_EQUAL(memcmp(response,(unsigned char*)"\xFF\xF9\xFF\xF9",4),0);
	
	unsigned char test2[]="this \xFF\xFF byte\r\n";
	remote.write(test2,14);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("this \xFF byte"));
	remote.read(response,4);
	
	unsigned char test3[]="\xFF\r\n\r\n";
	remote.write(test3,5);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("\n"));
	remote.read(response,4);
	
	unsigned char test4[]="\xFF\xFF\0\xFF\xFF\x0D\x0A";
	remote.write(test4,7);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("\xFF\xFF"));
	remote.read(response,4);
	
	//Если нам говорят что "они" будут использовать какую-то опцию
	unsigned char test5[]="\xFF\xFB\x01test\x0D\x0A";
	remote.write(test5,9);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("test"));
	remote.read(response,4);
	remote.read(test5,3);
	CPPUNIT_ASSERT_EQUAL(memcmp(test5,(unsigned char*)"\xFF\xFE\x01",3),0);
	
	//Если нам говорят чтобы мы включили чёта, мы говорим "нибуду"
	unsigned char test6[]="\xFF\xFD\x03test\x0D\x0A";
	remote.write(test6,9);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("test"));
	remote.read(response,4);
	remote.read(test6,3);
	CPPUNIT_ASSERT_EQUAL(memcmp(test6,(unsigned char*)"\xFF\xFC\x03",3),0);
	
	//Если нам говорят "мы нибудя" мы молчим.
	unsigned char test7[]="\xFF\xFC\x02test\x0D\x0A";
	remote.write(test7,9);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("test"));
	remote.read(response,4);
	
	//Если нам говорят "нинада" мы молчим
	unsigned char test8[]="\xFF\xFE\x04test\x0D\x0A";
	remote.write(test8,9);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("test"));
	remote.read(response,4);
	
	CPPUNIT_ASSERT_EQUAL(telnet.getMaxLineLength(),(unsigned int)160);
	telnet.setMaxLineLength(10);
	unsigned char test9[]="1234567891011\r\n";
	remote.write(test9,15);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("1234567891"));
	remote.read(response,4);
	telnet >> s1;
	CPPUNIT_ASSERT_EQUAL(s1,std::string("011"));
	remote.read(response,4);
}

void TelnetTest::testWriting(){
	Telnet rtelnet(remote);
	rtelnet << "\nThis is\r\n the test\n";
	usleep(50000);
	unsigned char buffer[23]={0};
	CPPUNIT_ASSERT_EQUAL(client.read(buffer,22),22);
	CPPUNIT_ASSERT_EQUAL(std::string(reinterpret_cast<char*>(buffer)),std::string("\r\nThis is\r\n the test\r\n"));
	
	rtelnet << "this \xFF" << Telnet::endl;
	usleep(50000);
	unsigned char buffer2[10]={0};
	CPPUNIT_ASSERT_EQUAL(client.read(buffer2,9),9);
	CPPUNIT_ASSERT_EQUAL(std::string(reinterpret_cast<char*>(buffer2)),std::string("this \xFF\xFF\r\n"));
	
	Telnet telnet(client);
	telnet << "This is \xFF the"  << Telnet::endl << "test\n";
	sleep(1);
	std::string ms1,ms2;
	rtelnet >> ms1 >> ms2;
	CPPUNIT_ASSERT_EQUAL(ms1, std::string("This is \xFF the"));
	CPPUNIT_ASSERT_EQUAL(ms2, std::string("test"));
	
}
