#ifndef __TELNETTEST_H__
#define __TELNETTEST_H__

#include <cppunit/extensions/HelperMacros.h>
#include <Server.h>
#include <Connection.h>
#include <SocketExceptions.h>

class TelnetTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE( TelnetTest );
	CPPUNIT_TEST( testReading );
	CPPUNIT_TEST( testWriting );
	CPPUNIT_TEST_SUITE_END();

private:
	Server server;
	Connection remote,client;
	int currentPort;
public:
	void setUp();
	void tearDown();
	
	void testReading();
	void testWriting();
};

#endif /* __TELNETTEST_H__ */

